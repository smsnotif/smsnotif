# SMSNotif notification channel for Laravel

[![Latest Version on Packagist](https://img.shields.io/packagist/v/smsnotif/smsnotif.svg?style=flat-square)](https://packagist.org/packages/smsnotif/smsnotif)
[![Total Downloads](https://img.shields.io/packagist/dt/smsnotif/smsnotif.svg?style=flat-square)](https://packagist.org/packages/smsnotif/smsnotif)
[![Software License](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](LICENSE.md)

This package makes it easy to send SMS notifications using the [smsnotif.id API](https://www.smsnotif.id) with Laravel.

[smsnotif.id](https://www.smsnotif.id) is an easy and affordable API for sending SMS to Indonesian mobile number. 

## Contents

- [SMSNotif notification channel for Laravel](#smsnotif-notification-channel-for-laravel)
  - [Contents](#contents)
  - [Installation](#installation)
    - [Setting up your SMSNotif Account](#setting-up-your-smsnotif-account)
  - [Usage](#usage)
  - [Security](#security)
  - [License](#license)
  - [Keywords](#keywords)


## Installation

You can install the package via composer:

```bash
composer require smsnotif/smsnotif
```

### Setting up your SMSNotif Account

1. Register at [smsnotif.id](https://www.smsnotif.id/register)
2. Click the `Tambah Kuota` button on your smsnotif dashboard and make payment.
3. Go to [API Keys](https://www.smsnotif.id/user/api-tokens) and Create an API Token.
4. Paste your API token, in your laravel `services.php` config file:

    ```php
    // config/services.php
    'smsnotif' => [
        'token' => 'YOUR_API_TOKEN',
    ],
    ```

## Usage

In every model you wish to be notifiable via SMSNotif, you must add a phone number property to that model accessible through a `routeNotificationForSmsnotif` method:

```php
class User extends Authenticatable
{
    use Notifiable;

    public function routeNotificationForSmsnotif()
    {
        return $this->mobile_number;
    }
}
```

You may now tell Laravel to send notifications to Smsnotif channels in the `via` method:

```php
// ...
use Smsnotif\SmsnotifChannel;

class OrderDeliveredNotification extends Notification
{
    public $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function via($notifiable)
    {
        return [SmsnotifChannel::class];
    }

    public function toSmsnotif($notifiable)
    {
        return "Laravel - Pesanan Anda No. {$this->order->order_number} telah dikirim. Terima Kasih");
    }
}
```

## Security

If you discover any security related issues, please email to support@smsnotif.id. Thank You!

## License

The MIT License (MIT). Please see [LICENSE](LICENSE.md) for more information.

## Keywords

SMS, Laravel Notification, SMS Gateway, SMS to Indonesia