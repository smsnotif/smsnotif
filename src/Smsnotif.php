<?php

namespace Smsnotif;

// use Exception;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Arr;
use Smsnotif\Exceptions\CouldNotSendNotification;

class Smsnotif
{
    /**
     * Discord API base URL.
     *
     * @var string
     */
    protected $baseUrl = 'https://www.smsnotif.id/api';

    /**
     * API HTTP client.
     *
     * @var \GuzzleHttp\Client
     */
    protected $httpClient;

    /**
     * Discord API token.
     *
     * @var string
     */
    protected $api_key;

    /**
     * @param \GuzzleHttp\Client $http
     * @param string $token
     */
    public function __construct(HttpClient $http, $api_key)
    {
        $this->httpClient = $http;
        $this->api_key = $api_key;
    }

    /**
     * Send a message to a Discord channel.
     *
     * @param string $to
     * @param string $message
     *
     * @return array
     */
    public function send($to, $message)
    {
        return $this->request('POST', $to, $message);
    }

    /**
     * Perform an HTTP request with the Discord API.
     *
     * @param string $verb
     * @param string $endpoint
     * @param array $data
     *
     * @return array
     *
     * @throws \NotificationChannels\Discord\Exceptions\CouldNotSendNotification
     */
    protected function request($verb, $to, $message)
    {
        $url = rtrim($this->baseUrl, '/').'/messages';

        try {
            $response = $this->httpClient->request($verb, $url, [
                'headers' => [
                    'Authorization' => 'Bearer '.$this->api_key,
                ],
                'json' => [
                    'to' => $to,
                    'message' => $message
                ],
            ]);
        } catch (RequestException $exception) {
            if ($response = $exception->getResponse()) {
                throw CouldNotSendNotification::serviceRespondedWithAnHttpError($response, $response->getStatusCode(), $exception);
            }

            throw CouldNotSendNotification::serviceCommunicationError($exception);
        } catch (Exception $exception) {
            throw CouldNotSendNotification::serviceCommunicationError($exception);
        }
        
        $body = json_decode($response->getBody(), true);

        // if (Arr::get($body, 'code', 0) > 0) {
        //     throw CouldNotSendNotification::serviceRespondedWithAnApiError($body, $body['code']);
        // }

        return $body;
    }
}