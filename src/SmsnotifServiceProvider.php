<?php

namespace Smsnotif;

use GuzzleHttp\Client as HttpClient;
use Illuminate\Support\ServiceProvider;

/**
 * Class SmsnotifServiceProvider.
 */
class SmsnotifServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot(): void
    {
        // $this->app->when(TelegramChannel::class)
        //     ->needs(Telegram::class)
        //     ->give(static function () {
        //         return new Telegram(
        //             config('services.telegram-bot-api.token'),
        //             app(HttpClient::class),
        //             config('services.telegram-bot-api.base_uri')
        //         );
        //     });

        $api_key = $this->app->make('config')->get('services.smsnotif.api_key');

        $this->app->when(Smsnotif::class)
            ->needs('$api_key')
            ->give($api_key);
    }
}