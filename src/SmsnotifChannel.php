<?php

namespace Smsnotif;

use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class SmsnotifChannel {
    
    /**
     * @var \Smsnotif\Smsnotif
     */
    protected $smsnotif;

    /**
     * @param \Smsnotif\Smsnotif $smsnotif
     */
    public function __construct(Smsnotif $smsnotif)
    {
        $this->smsnotif = $smsnotif;
    }

    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        if (! $to = $notifiable->routeNotificationFor('smsnotif')) {
            return;
        }

        $message = $notification->toSmsnotif($notifiable);

        return $this->smsnotif->send($to, $message);

    }

}